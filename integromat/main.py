"""Main integromat script."""

import json
from typing import IO, Any, Dict, Iterable, NamedTuple, Optional, Sequence, cast

import click

from integromat import __version__
from integromat.client import Client

# List of keys which contain connection data.
CONNECTION_KEYS = ("__IMTCONN__", "account")


class NewItem(NamedTuple):
    """Representation of a new item in a switch."""

    id: int
    label: Optional[str]


@click.group()
@click.version_option(__version__)
def main() -> None:
    """Main command group."""


def iter_modules(flow: Sequence[Dict[str, Any]]) -> Iterable[Dict[str, Any]]:
    """Iterate through modules in flow."""
    for module in flow:
        yield module
        if "routes" in module:
            for route in module["routes"]:
                yield from iter_modules(route["flow"])


def get_parameters(module: Dict[str, Any]) -> Sequence[Dict[str, Any]]:
    """Return parameters for a module."""
    return cast(Sequence[Dict[str, Any]], module["metadata"].get("parameters", ()))


def get_connection_params(module: Dict[str, Any]) -> Iterable[str]:
    """Return names of connection parameters."""
    for param in get_parameters(module):
        if param["type"].startswith("account"):
            yield param["name"]


def get_connections(blueprint: Dict[str, Any]) -> Dict[int, str]:
    """Return connections from the blueprint."""
    connections = {}
    for module in iter_modules(blueprint["flow"]):
        for param in get_connection_params(module):
            if "parameters" in module["metadata"]["restore"]:
                # New version
                connections[module["parameters"][param]] = module["metadata"]["restore"]["parameters"][param]["label"]
            else:
                # Old version
                connections[module["parameters"][param]] = module["metadata"]["restore"][param]["label"]
    return connections


def get_bases(blueprint: Dict[str, Any]) -> Dict[int, str]:
    """Return bases from the blueprint."""
    bases = {}
    for module in iter_modules(blueprint["flow"]):
        if "base" in (module.get("mapper") or {}):
            bases[module["mapper"]["base"]] = module["metadata"]["restore"]["base"]["label"]
    return bases


@main.command()
@click.argument("file", type=click.File("r"))
def connections(file: IO) -> None:
    """Print all connections in a blueprint."""
    blueprint = json.load(file)
    for id, name in get_connections(blueprint).items():
        click.echo("{}: {}".format(id, name))


@main.command()
@click.argument("file", type=click.File("r"))
def bases(file: IO) -> None:
    """Print all bases in a blueprint."""
    blueprint = json.load(file)
    for id, name in get_bases(blueprint).items():
        click.echo("{}: {}".format(id, name))


@main.command()
@click.argument("file", type=click.File("r"))
def info(file: IO) -> None:
    """Print basic info about blueprint."""
    data = json.load(file)
    click.echo("Blueprint: {}".format(data["name"]))
    click.echo()
    for module in iter_modules(data["flow"]):
        click.echo("{} [{}]".format(module["module"], module["id"]))


@main.command()
@click.argument("file", type=click.File("r"))
@click.option(
    "--input", type=click.File("r"), default="-", show_default=True, help="A file with the input blueprint."
)
@click.option(
    "--output", type=click.File("w"), default="-", show_default=True, help="A file for the output blueprint."
)
def switch(file: IO, input: IO, output: IO) -> None:
    """Switch connections in a blueprint."""
    connection_map = {}
    for conn_data in json.load(file):
        connection_map[conn_data["from"]] = NewItem(conn_data["to"], conn_data.get("label"))

    blueprint = json.load(input)
    for module in iter_modules(blueprint["flow"]):
        for param in get_connection_params(module):
            if module["parameters"][param] in connection_map:
                new_connection = connection_map[module["parameters"][param]]
                module["parameters"][param] = new_connection.id
                if new_connection.label:
                    if "parameters" in module["metadata"]["restore"]:
                        # New version
                        module["metadata"]["restore"]["parameters"][param]["label"] = new_connection.label
                    else:
                        # Old version
                        module["metadata"]["restore"][param]["label"] = new_connection.label
    json.dump(blueprint, output, indent=4, ensure_ascii=False)


@main.command()
@click.argument("file", type=click.File("r"))
@click.option(
    "--input", type=click.File("r"), default="-", show_default=True, help="A file with the input blueprint."
)
@click.option(
    "--output", type=click.File("w"), default="-", show_default=True, help="A file for the output blueprint."
)
def switch_base(file: IO, input: IO, output: IO) -> None:
    """Switch bases in a blueprint."""
    base_map = {}
    for conn_data in json.load(file):
        base_map[conn_data["from"]] = NewItem(conn_data["to"], conn_data.get("label"))

    blueprint = json.load(input)
    for module in iter_modules(blueprint["flow"]):
        if "base" in (module.get("mapper") or {}):
            if module["mapper"]["base"] in base_map:
                new_base = base_map[module["mapper"]["base"]]
                module["mapper"]["base"] = new_base.id
                if new_base.label:
                    module["metadata"]["restore"]["base"]["label"] = new_base.label
    json.dump(blueprint, output)


@main.group()
def client() -> None:
    """Client command group."""


@client.command()
@click.option("--username", prompt=True, help="Login username.")
@click.option("--password", prompt=True, hide_input=True, help="Login password.")
def list_companies(username: str, password: str) -> None:
    """List companies."""
    cli = Client(username, password)
    cli.login()
    for company in cli.list_companies():
        click.echo("{} [{}]".format(company["name"], company["id"]))


@client.command()
@click.argument("company-id", type=int)
@click.option("--username", prompt=True, help="Login username.")
@click.option("--password", prompt=True, hide_input=True, help="Login password.")
def list_scenarios(company_id: int, username: str, password: str) -> None:
    """List scenarios for a company."""
    cli = Client(username, password)
    cli.login()
    for company in cli.list_scenarios(company_id):
        click.echo("{} [{}] {}".format(company["name"], company["id"], company["folder"]))


@client.command()
@click.argument("scenario-id", type=int)
@click.option("--username", prompt=True, help="Login username.")
@click.option("--password", prompt=True, hide_input=True, help="Login password.")
@click.option(
    "--output", type=click.File("w"), default="-", show_default=True, help="A file for the output blueprint."
)
def pull_blueprint(scenario_id: int, username: str, password: str, output: IO) -> None:
    """Pull and print blueprint for a scenario."""
    cli = Client(username, password)
    cli.login()
    json.dump(cli.pull_blueprint(scenario_id), output, indent=4, ensure_ascii=False)
    # Print newline at the end of JSON file.
    output.write("\n")


@client.command()
@click.argument("scenario-id", type=int)
@click.option("--username", prompt=True, help="Login username.")
@click.option("--password", prompt=True, hide_input=True, help="Login password.")
@click.option("--input", type=click.File("r"), default="-", show_default=True, help="A file with the blueprint.")
def push_blueprint(scenario_id: int, username: str, password: str, input: IO) -> None:
    """Push blueprint for a scenario."""
    cli = Client(username, password)
    cli.login()
    cli.push_blueprint(scenario_id, json.load(input))


if __name__ == "__main__":
    main()
