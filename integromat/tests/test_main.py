import json
from typing import Any, Dict, Sequence
from unittest import TestCase
from unittest.mock import sentinel

import responses
from click.testing import CliRunner

from integromat.main import (
    bases,
    connections,
    get_bases,
    get_connection_params,
    get_connections,
    get_parameters,
    info,
    iter_modules,
    list_companies,
    list_scenarios,
    pull_blueprint,
    push_blueprint,
    switch,
    switch_base,
)


class IterModulesTest(TestCase):
    def test_empty(self):
        self.assertEqual(tuple(iter_modules(())), ())

    def test_simple(self):
        module1 = {"module": sentinel.module1}
        module2 = {"module": sentinel.module2}
        module3 = {"module": sentinel.module3}
        flow = [module1, module2, module3]
        self.assertEqual(tuple(iter_modules(flow)), (module1, module2, module3))

    def test_route(self):
        module1 = {"module": sentinel.module1}
        module2 = {"module": sentinel.module2}
        route = {"routes": [{"flow": [module1, module2]}]}
        flow = [route]
        self.assertEqual(tuple(iter_modules(flow)), (route, module1, module2))


class GetParametersTest(TestCase):
    def test_no_parameters(self):
        module = {"metadata": {}}
        self.assertEqual(get_parameters(module), ())

    def test_empty(self):
        module = {"metadata": {"parameters": []}}
        self.assertEqual(get_parameters(module), [])

    def test_params(self):
        module = {"metadata": {"parameters": [sentinel.param1, sentinel.param2]}}
        self.assertEqual(get_parameters(module), [sentinel.param1, sentinel.param2])


class GetConnectionParamsTest(TestCase):
    def test_no_parameters(self):
        module = {"metadata": {}}
        self.assertEqual(tuple(get_connection_params(module)), ())

    def test_empty(self):
        module = {"metadata": {"parameters": []}}
        self.assertEqual(tuple(get_connection_params(module)), ())

    def test_other_parameter(self):
        module = {"metadata": {"parameters": [{"type": "other"}]}}
        self.assertEqual(tuple(get_connection_params(module)), ())

    def test_connection(self):
        module = {"metadata": {"parameters": [{"type": "account:airtable", "name": sentinel.name}]}}
        self.assertEqual(tuple(get_connection_params(module)), (sentinel.name,))

    def test_connection_legacy(self):
        module = {"metadata": {"parameters": [{"type": "account", "name": sentinel.name}]}}
        self.assertEqual(tuple(get_connection_params(module)), (sentinel.name,))


class GetConnectionsTest(TestCase):
    def test_empty(self):
        # Test blueprint with no modules.
        blueprint = {"flow": []}
        self.assertEqual(get_connections(blueprint), {})

    def test_no_parameters(self):
        # Test module with no parameter.
        module = {"metadata": {"parameters": []}}
        blueprint = {"flow": [module]}
        self.assertEqual(get_connections(blueprint), {})

    def test_other_parameter(self):
        # Test module with other parameter than connection.
        module = {"metadata": {"parameters": [{"type": "other"}]}}
        blueprint = {"flow": [module]}
        self.assertEqual(get_connections(blueprint), {})

    def test_connection(self):
        # Test module with a connection.
        metadata = {
            "parameters": [{"type": "account:airtable", "name": "conn"}],
            "restore": {"parameters": {"conn": {"label": "Stasis leak"}}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        self.assertEqual(get_connections(blueprint), {16: "Stasis leak"})

    def test_connection_multiple(self):
        # Test multiple modules with a connection.
        metadata = {
            "parameters": [{"type": "account:airtable", "name": "conn"}],
            "restore": {"parameters": {"conn": {"label": "Stasis leak"}}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module, module, module]}
        self.assertEqual(get_connections(blueprint), {16: "Stasis leak"})

    def test_connection_legacy(self):
        # Test module with a connection.
        metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Stasis leak"}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        self.assertEqual(get_connections(blueprint), {16: "Stasis leak"})

    def test_connection_legacy_multiple(self):
        # Test multiple modules with a connection.
        metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Stasis leak"}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module, module, module]}
        self.assertEqual(get_connections(blueprint), {16: "Stasis leak"})


class GetBasesTest(TestCase):
    def test_empty(self):
        # Test blueprint with no modules.
        blueprint = {"flow": []}
        self.assertEqual(get_bases(blueprint), {})

    def test_no_mapper(self):
        # Test module with no mapper.
        module = {}
        blueprint = {"flow": [module]}
        self.assertEqual(get_bases(blueprint), {})

    def test_null_mapper(self):
        # Test module with null mapper.
        module = {"mapper": None}
        blueprint = {"flow": [module]}
        self.assertEqual(get_bases(blueprint), {})

    def test_no_base(self):
        # Test module with no base.
        module = {"mapper": {}}
        blueprint = {"flow": [module]}
        self.assertEqual(get_bases(blueprint), {})

    def test_base(self):
        # Test module with a base.
        metadata = {
            "restore": {"base": {"label": "Red Dwarf"}},
        }
        module = {"mapper": {"base": "red-dwarf"}, "metadata": metadata}
        blueprint = {"flow": [module]}
        self.assertEqual(get_bases(blueprint), {"red-dwarf": "Red Dwarf"})

    def test_base_multiple(self):
        # Test module with a base.
        metadata = {
            "restore": {"base": {"label": "Red Dwarf"}},
        }
        module = {"mapper": {"base": "red-dwarf"}, "metadata": metadata}
        blueprint = {"flow": [module, module, module]}
        self.assertEqual(get_bases(blueprint), {"red-dwarf": "Red Dwarf"})


class ConnectionsTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_connections(self):
        metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Stasis leak"}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        with self.runner.isolated_filesystem():
            with open("blueprint.json", "w") as file:
                json.dump(blueprint, file)

            result = self.runner.invoke(connections, ["blueprint.json"])

        output = "16: Stasis leak\n"
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, output)


class BasesTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_bases(self):
        metadata = {
            "restore": {"base": {"label": "Red Dwarf"}},
        }
        module = {"mapper": {"base": "red-dwarf"}, "metadata": metadata}
        blueprint = {"flow": [module]}
        with self.runner.isolated_filesystem():
            with open("blueprint.json", "w") as file:
                json.dump(blueprint, file)

            result = self.runner.invoke(bases, ["blueprint.json"])

        output = "red-dwarf: Red Dwarf\n"
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, output)


class InfoTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_info(self):
        data = {"name": "Blue pill", "flow": [{"module": "TakePill", "id": "42"}]}
        with self.runner.isolated_filesystem():
            with open("blueprint.json", "w") as file:
                json.dump(data, file)

            result = self.runner.invoke(info, ["blueprint.json"])

        output = "Blueprint: Blue pill\n\nTakePill [42]\n"
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, output)


class SwitchTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def _test(self, blueprint: Dict[str, Any], connection_map: Sequence, output: Dict[str, Any]) -> None:
        with self.runner.isolated_filesystem():
            with open("blueprint.json", "w") as file:
                json.dump(blueprint, file)
            with open("map.json", "w") as file:
                json.dump(connection_map, file)

            result = self.runner.invoke(switch, ["map.json", "--input", "blueprint.json"])

        self.assertEqual(result.exit_code, 0)
        self.assertEqual(json.loads(result.output), output)

    def test_no_modules(self):
        # Test blueprint with no modules.
        blueprint = {"flow": []}
        connection_map = []
        self._test(blueprint, connection_map, blueprint)

    def test_no_connection(self):
        # Test module without a connection in blueprint.
        module = {"metadata": {"parameters": []}}
        blueprint = {"flow": [module]}
        connection_map = []
        self._test(blueprint, connection_map, blueprint)

    def test_empty_map(self):
        # Test empty map.
        metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Stasis leak"}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        connection_map = []
        self._test(blueprint, connection_map, blueprint)

    def test_switch(self):
        # Test switch.
        metadata = {
            "parameters": [{"type": "account:airtable", "name": "conn"}],
            "restore": {"parameters": {"conn": {"label": "Stasis leak"}}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        connection_map = [{"from": 16, "to": 2583}]
        out_module = {"metadata": metadata, "parameters": {"conn": 2583}}
        output = {"flow": [out_module]}
        self._test(blueprint, connection_map, output)

    def test_switch_label(self):
        # Test switch with label.
        metadata = {
            "parameters": [{"type": "account:airtable", "name": "conn"}],
            "restore": {"parameters": {"conn": {"label": "Stasis leak"}}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        connection_map = [{"from": 16, "to": 2583, "label": "Showers"}]
        out_metadata = {
            "parameters": [{"type": "account:airtable", "name": "conn"}],
            "restore": {"parameters": {"conn": {"label": "Showers"}}},
        }
        out_module = {"metadata": out_metadata, "parameters": {"conn": 2583}}
        output = {"flow": [out_module]}
        self._test(blueprint, connection_map, output)

    def test_switch_legacy(self):
        # Test switch.
        metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Stasis leak"}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        connection_map = [{"from": 16, "to": 2583}]
        out_module = {"metadata": metadata, "parameters": {"conn": 2583}}
        output = {"flow": [out_module]}
        self._test(blueprint, connection_map, output)

    def test_switch_legacy_label(self):
        # Test switch with label.
        metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Stasis leak"}},
        }
        module = {"metadata": metadata, "parameters": {"conn": 16}}
        blueprint = {"flow": [module]}
        connection_map = [{"from": 16, "to": 2583, "label": "Showers"}]
        out_metadata = {
            "parameters": [{"type": "account", "name": "conn"}],
            "restore": {"conn": {"label": "Showers"}},
        }
        out_module = {"metadata": out_metadata, "parameters": {"conn": 2583}}
        output = {"flow": [out_module]}
        self._test(blueprint, connection_map, output)


class SwitchBaseTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def _test(self, blueprint: Dict[str, Any], base_map: Sequence, output: Dict[str, Any]) -> None:
        with self.runner.isolated_filesystem():
            with open("blueprint.json", "w") as file:
                json.dump(blueprint, file)
            with open("map.json", "w") as file:
                json.dump(base_map, file)

            result = self.runner.invoke(switch_base, ["map.json", "--input", "blueprint.json"])

        self.assertEqual(result.exit_code, 0)
        self.assertEqual(json.loads(result.output), output)

    def test_no_modules(self):
        # Test blueprint with no modules.
        blueprint = {"flow": []}
        base_map = []
        self._test(blueprint, base_map, blueprint)

    def test_no_mapper(self):
        # Test module without a mapper in blueprint.
        module = {}
        blueprint = {"flow": [module]}
        base_map = []
        self._test(blueprint, base_map, blueprint)

    def test_null_mapper(self):
        # Test module a null mapper in blueprint.
        module = {"mapper": None}
        blueprint = {"flow": [module]}
        base_map = []
        self._test(blueprint, base_map, blueprint)

    def test_no_base(self):
        # Test module without a base in blueprint.
        module = {"mapper": {}}
        blueprint = {"flow": [module]}
        base_map = []
        self._test(blueprint, base_map, blueprint)

    def test_empty_map(self):
        # Test empty map.
        metadata = {
            "restore": {"base": {"label": "Red Dwarf"}},
        }
        module = {"mapper": {"base": "red-dwarf"}, "metadata": metadata}
        blueprint = {"flow": [module]}
        base_map = []
        self._test(blueprint, base_map, blueprint)

    def test_switch(self):
        # Test switch.
        metadata = {
            "restore": {"base": {"label": "Red Dwarf"}},
        }
        module = {"mapper": {"base": "red-dwarf"}, "metadata": metadata}
        blueprint = {"flow": [module]}
        base_map = [{"from": "red-dwarf", "to": "nova-5"}]
        out_module = {"mapper": {"base": "nova-5"}, "metadata": metadata}
        output = {"flow": [out_module]}
        self._test(blueprint, base_map, output)

    def test_switch_label(self):
        # Test switch with label.
        metadata = {
            "restore": {"base": {"label": "Red Dwarf"}},
        }
        module = {"mapper": {"base": "red-dwarf"}, "metadata": metadata}
        blueprint = {"flow": [module]}
        base_map = [{"from": "red-dwarf", "to": "nova-5", "label": "Nova 5"}]
        out_metadata = {
            "restore": {"base": {"label": "Nova 5"}},
        }
        out_module = {"mapper": {"base": "nova-5"}, "metadata": out_metadata}
        output = {"flow": [out_module]}
        self._test(blueprint, base_map, output)


class ListCompaniesTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_list_companies(self):
        data = {"response": [{"name": "JMC", "id": 42}]}
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, "https://www.integromat.com/api/login")
            rsps.add(responses.GET, "https://www.integromat.com/api/companies/list", json=data)

            result = self.runner.invoke(list_companies, ["--username", "rimmer", "--password", "Gazpacho!"])

        output = "JMC [42]\n"
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, output)


class ListScenariosTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_list_scenarios(self):
        body = """
            <html>
                <body>
                    <div class="scenarios">
                        <a class="scenario" data-id="40">
                            <div class="list-group-title">Rusty Gate</div>
                            <span class="scenario-folder">Blackmail</span>
                        </a>
                    </div>
                </body>
            </html>
        """
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, "https://www.integromat.com/api/login")
            rsps.add(responses.GET, "https://www.integromat.com/scenarios/42", body=body)

            result = self.runner.invoke(list_scenarios, ["42", "--username", "rimmer", "--password", "Gazpacho!"])

        output = "Rusty Gate [40] Blackmail\n"
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, output)


class PullBlueprintTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_pull_blueprint(self):
        data = {"response": {"blueprint": {"name": "Rusty Gate"}}}
        with responses.RequestsMock() as rsps:
            rsps.add(responses.POST, "https://www.integromat.com/api/login")
            rsps.add(responses.GET, "https://www.integromat.com/api/scenario/40/blueprint", json=data)

            result = self.runner.invoke(pull_blueprint, ["40", "--username", "rimmer", "--password", "Gazpacho!"])

        output = '{\n    "name": "Rusty Gate"\n}\n'
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, output)


class PushBlueprintTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_push_blueprint(self):
        scheduling = {"type": "indefinitely", "interval": 900}
        data = {"response": {"scheduling": scheduling}}
        blueprint = {"name": "Rusty Gate"}
        with self.runner.isolated_filesystem():
            with open("blueprint.json", "w") as file:
                json.dump(blueprint, file)

            with responses.RequestsMock() as rsps:
                rsps.add(responses.POST, "https://www.integromat.com/api/login")
                rsps.add(responses.GET, "https://www.integromat.com/api/scenario/40/blueprint", json=data)
                rsps.add(responses.POST, "https://www.integromat.com/api/scenario/40/save")

                result = self.runner.invoke(
                    push_blueprint,
                    ["40", "--username", "rimmer", "--password", "Gazpacho!", "--input", "blueprint.json"],
                )

        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.output, "")
