import json
from unittest import TestCase
from unittest.mock import sentinel

import responses
from responses.matchers import header_matcher, json_params_matcher, urlencoded_params_matcher

from integromat.client import Client


class ClientTest(TestCase):
    def test_login(self):
        client = Client("rimmer", "Gazpacho!")
        req_data = {"email": "rimmer", "password": "Gazpacho!"}
        with responses.RequestsMock() as rsps:
            match = [urlencoded_params_matcher(req_data), header_matcher({"Referer": "https://www.integromat.com/"})]
            rsps.add(responses.POST, "https://www.integromat.com/api/login", match=match)

            client.login()

    def test_list_companies(self):
        client = Client(sentinel.username, sentinel.password)
        data = {"response": [{"name": "JMC"}]}

        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, "https://www.integromat.com/api/companies/list", json=data)

            self.assertEqual(client.list_companies(), [{"name": "JMC"}])

    def test_list_scenarios(self):
        client = Client(sentinel.username, sentinel.password)
        body = """
            <html>
                <body>
                    <div class="scenarios">
                        <a class="scenario" data-id="40">
                            <div class="list-group-title">Rusty Gate</div>
                            <span class="scenario-folder">Blackmail</span>
                        </a>
                    </div>
                </body>
            </html>
        """

        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, "https://www.integromat.com/scenarios/42", body=body)

            self.assertEqual(client.list_scenarios(42), [{"name": "Rusty Gate", "id": 40, "folder": "Blackmail"}])

    def test_pull_blueprint(self):
        client = Client(sentinel.username, sentinel.password)
        data = {"response": {"blueprint": {"name": "Rusty Gate"}}}

        with responses.RequestsMock() as rsps:
            rsps.add(responses.GET, "https://www.integromat.com/api/scenario/40/blueprint", json=data)

            self.assertEqual(client.pull_blueprint(40), {"name": "Rusty Gate"})

    def test_push_blueprint(self):
        client = Client(sentinel.username, sentinel.password)
        scheduling = {"type": "indefinitely", "interval": 900}
        blueprint = {"name": "Rusty Gate"}
        data = {"blueprint": json.dumps(blueprint), "scheduling": json.dumps(scheduling)}

        with responses.RequestsMock() as rsps:
            # Fetch scheduling
            rsps.add(
                responses.GET,
                "https://www.integromat.com/api/scenario/40/blueprint",
                json={"response": {"scheduling": scheduling}},
            )
            match = json_params_matcher(data)
            rsps.add(responses.POST, "https://www.integromat.com/api/scenario/40/save", match=[match])

            client.push_blueprint(40, blueprint)
