"""Integromat API client."""

import json
from typing import Dict, Sequence, cast

from bs4 import BeautifulSoup
from requests import Session


class Client:
    """Client for integromat API."""

    def __init__(self, username: str, password: str):
        """Initialize client."""
        self.session = Session()
        self.username = username
        self.password = password

    def login(self) -> None:
        """Log in to the API."""
        # Set up session
        response = self.session.post(
            "https://www.integromat.com/api/login",
            data={"email": self.username, "password": self.password},
            headers={"Referer": "https://www.integromat.com/"},
        )
        response.raise_for_status()

    def list_companies(self) -> Sequence[Dict]:
        """Return a list of companies."""
        response = self.session.get("https://www.integromat.com/api/companies/list")
        response.raise_for_status()
        return cast(Sequence[Dict], response.json()["response"])

    def list_scenarios(self, company_id: int) -> Sequence[Dict]:
        """Return a list of scenarios."""
        response = self.session.get("https://www.integromat.com/scenarios/{}".format(company_id))
        response.raise_for_status()
        html = BeautifulSoup(response.text, "html.parser")
        scenarios = []
        for scenario_html in html.select("div.scenarios a.scenario"):
            scenario = {
                "name": scenario_html.select("div.list-group-title")[0].text,
                "id": int(scenario_html.attrs["data-id"]),
                "folder": scenario_html.select("span.scenario-folder")[0].text,
            }
            scenarios.append(scenario)
        return scenarios

    def _pull_blueprint(self, scenario_id: int) -> Dict:
        """Return a complate blueprint data of a scenario."""
        response = self.session.get("https://www.integromat.com/api/scenario/{}/blueprint".format(scenario_id))
        response.raise_for_status()
        return cast(Dict, response.json()["response"])

    def pull_blueprint(self, scenario_id: int) -> Dict:
        """Return a blueprint of a scenario."""
        return cast(Dict, self._pull_blueprint(scenario_id)["blueprint"])

    def push_blueprint(self, scenario_id: int, blueprint: Dict) -> None:
        """Save a blueprint to a scenario."""
        # Scheduling is required to save the blueprint. Fetch current setup.
        scheduling = self._pull_blueprint(scenario_id)["scheduling"]
        data = {
            "blueprint": json.dumps(blueprint, ensure_ascii=False),
            "scheduling": json.dumps(scheduling, ensure_ascii=False),
        }
        response = self.session.post(
            "https://www.integromat.com/api/scenario/{}/save".format(scenario_id),
            json=data,
            headers={"Referer": "https://www.integromat.com/"},
        )
        response.raise_for_status()
